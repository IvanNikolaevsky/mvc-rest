<?php 

defined('APP_PATH')
    || define('APP_PATH', realpath(dirname(__FILE__) . '/../application'));

defined('DS')
    || define('DS', DIRECTORY_SEPARATOR );



function autoload_class($class_name) {
    $directories = array(
        APP_PATH . DS,
        APP_PATH . DS . 'controllers' . DS,
        APP_PATH . DS . 'models' . DS,
        APP_PATH . DS . 'core' . DS,
    );
    foreach ($directories as $directory) {
        $filename = $directory . $class_name . '.php';
        if (is_file($filename)) {
            require($filename);
            break;
        }
    }
}


spl_autoload_register('autoload_class');


$bootstrap = new Bootstrap();
$bootstrap->run();