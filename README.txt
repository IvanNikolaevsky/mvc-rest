
Для правильной работы приложения необходимо:
- распаковать архив с файлами в публичную директорию
- прописать путь к индексному файлу в vhosts:
        <VirtualHost *:80>
            ServerName tz.local
            DocumentRoot /path/to/tz/public

            <Directory /path/to/tz/public>
                DirectoryIndex index.php
                AllowOverride All
                Order allow,deny
                Allow from all
            </Directory>
        </VirtualHost>
- создать базу данных для приложения и импортировать в нее дамп docs/db-dump.sql
- при желании импортировать тестовые данные docs/test-data.sql
- прописать подключение к базе данных в классе Config (core/Config.php)

Приложение принимает запросы только в формате json, поэтому с запросом необходимо отправлять  заголовoк:
Accept: application/json


View в приложении реализован формально, т.к json не требует особых шаблонов отображения. Папка views добавлена для наглядности. 
