DROP TABLE IF EXISTS `ADDRESS`;
CREATE TABLE `ADDRESS` (
  `ADDRESSID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The unique address ID.',
  `LABEL` varchar(100) NOT NULL COMMENT 'The name of the person or organisation to which the address belongs.',
  `STREET` varchar(100) NOT NULL COMMENT 'The name of the street.',
  `HOUSENUMBER` varchar(10) NOT NULL COMMENT 'The house number (and any optional additions).',
  `POSTALCODE` varchar(6) NOT NULL COMMENT 'The postal code for the address.',
  `CITY` varchar(100) NOT NULL COMMENT 'The city in which the address is located.',
  `COUNTRY` varchar(100) NOT NULL COMMENT 'The country in which the address is located.',
  PRIMARY KEY (`ADDRESSID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='A physical address belonging to a person or organisation.';

