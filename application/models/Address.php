<?php


class Address extends AbstractModel
{
    protected $_name = "ADDRESS";
    protected $_primaryKey = "ADDRESSID";
    
    protected $_validFields = array(
        'LABEL'=>100,
        'STREET'=>100,
        'HOUSENUMBER'=>10,
        'POSTALCODE'=>6,
        'CITY'=>100,
        'COUNTRY'=>100
    );

}