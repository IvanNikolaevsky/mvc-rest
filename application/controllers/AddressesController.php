<?php

class AddressesController extends AbstractController {

    const PARAM_ID = 1;

    public function __construct() {
        $this->model = new Address();
        $this->view = new View();
        
    }
    
    


    public function getAction() {

        switch (count($this->getRequest()->getUrlElements())) {
            case 1:
                $this->view->json($this->model->fetchAll());
                break;
            case 2:
                $id = (int) $this->getRequest()->getUrlElement(self::PARAM_ID);
                $address = $this->model->getById($id);
                if ($address) {
                    $this->view->json($address);
                } else {
                    $this->getResponse()->setHeader('HTTP/1.1 404 Not Found');
                }

                break;
            default :
                $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
        }
    }

    public function postAction() {
        if (count($this->getRequest()->getUrlElements()) == 1) {
            $params = $this->getRequest()->getParams();
            if ($this->model->isValid($params)) {
                $id = $this->model->insert($params);
                $this->getResponse()->setHeader('HTTP/1.1 201 Created');
                $this->getResponse()->setHeader('Location: /addresses/' . $id);
            } else {
                $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
                $this->view->json($this->model->getErrors());
            }
        } else {
            $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
        }
    }

    public function putAction() {

        switch (count($this->getRequest()->getUrlElements())) {
            case 2:
                $id = (int) $this->getRequest()->getUrlElement(self::PARAM_ID);
                if (!$this->model->getById($id)) {
                    $this->getResponse()->setHeader('HTTP/1.1 404 Not Found');
                    return false;
                }
                $params = $this->getRequest()->getParams();
                if ($this->model->isValid($params)) {
                    $this->model->update($params, $id);
                    $this->getResponse()->setHeader('HTTP/1.1 200 OK');
                } else {
                    $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
                    $this->view->json($this->model->getErrors());
                }
                break;
            default :
                $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
        }
    }

    public function deleteAction() {
        switch (count($this->getRequest()->getUrlElements())) {
            case 1:
                $this->model->delete();
                $this->getResponse()->setHeader('HTTP/1.1 204 No Content');
                break;
            case 2:
                $id = (int) $this->getRequest()->getUrlElement(self::PARAM_ID);
                if($this->model->getById($id)){
                    $this->model->delete($id);
                    $this->getResponse()->setHeader('HTTP/1.1 204 No Content');
                }else{
                    $this->getResponse()->setHeader('HTTP/1.1 404 Not Found');
                }                    
                break;
            default :
                $this->getResponse()->setHeader('HTTP/1.1 400 Bad Request');
        }
    }

}