<?php

abstract class AbstractModel
{
    protected $_connection;
    
    protected $_name;
    
    protected $_primaryKey;
    
    protected $_validFields = array();
    
    protected $_errors = array();    
    
    public function __construct(){
        $this->_connection = mysqli_connect(Config::MYSQL_HOST,Config::MYSQL_USER,Config::MYSQL_PASSWORD,Config::MYSQL_DATABASE);
    }
    
    
    
    
    
    public function fetchAll(){
        $select = mysqli_query($this->_connection, "SELECT * FROM " . $this->_name);
        $result = array();
        while ($row = mysqli_fetch_assoc($select)){
            $result[] = (object) $row;
        }
        return $result;
    }
    
    public function getById($id){
        $select = mysqli_query($this->_connection, "SELECT * FROM " . $this->_name . " WHERE " . $this->_primaryKey . " = {$id}");
        return mysqli_fetch_assoc($select);
    }
    
    
    public function insert($data){
        $data = $this->_prepareInsert($data);
        mysqli_query($this->_connection, "INSERT INTO " . $this->_name . " (" . $data['keys'] . ") VALUES (" . $data['values'] . ")");
        return mysqli_insert_id($this->_connection);        
    }
    



    public function update($data,$id){
        $data = $this->_prepareUpdate($data);
        mysqli_query($this->_connection, "UPDATE " . $this->_name . " SET " . $data . " WHERE " . $this->_primaryKey . " = {$id}");      
    }
    
    

    
    public function delete($id=null){
        $where = "";
        $id = intval($id);
        if($id){
            $where = " WHERE " . $this->_primaryKey . " = {$id}";            
        }
        mysqli_query($this->_connection, "DELETE FROM " . $this->_name . " " . $where); 
    }    
    
    
    
    protected function _prepareInsert($data){
        $keys = implode(",",array_keys($data));
        $values = "'" .implode("','",$this->_filter(array_values($data))) . "'";
        return array(
            'keys'=>$keys,
            'values'=>$values
        );

    }
    
    
    protected function _filter($data){
        $data = array_map(function($val){
            return addslashes(strip_tags($val));
        },$data);
        return $data;
    }
    
    protected function _prepareUpdate($data){
        $list = array();
        foreach ($this->_filter($data) as $key=>$value){
            $list[] = $key . "='" . $value . "'";
        }
        return implode(",",$list);
    }   
    
    
    
    public function isValid($data){
        $isValid = true;
        foreach ($data as $key=>$value){
            if(array_key_exists($key, $this->_validFields)){
                if(strlen($value) > $this->_validFields[$key]){
                    $isValid = false;
                    $this->_errors[] = $key . " is to long";
                }
            }else{
                $isValid = false;
                $this->_errors[] = $key . " is not valid name";
                
            }
        }
        if(!count($data)){
                $isValid = false;
                $this->_errors[] = "no data";            
        }
        return $isValid;
    }    
    
    
    public function getErrors(){
        return $this->_errors;
    }    
}