<?php

class Request {

    const METHOD_GET = "GET";
    const METHOD_POST = "POST";
    const METHOD_PUT = "PUT";
    const METHOD_DELETE = "DELETE";
    const CONTROLLER_INDEX = 0;

    protected $_method;
    protected $_url;
    protected $_urlElements = array();
    protected $_params;
    protected $_headers;

    public function __construct() {
        $this->setUrlElements()
                ->setMethod()
                ->setParams()
                ->setHeaders();
    }

    protected function setUrlElements() {
        if (isset($_SERVER['PATH_INFO'])) {
            $this->_urlElements = explode('/', trim($_SERVER['PATH_INFO'], '/'));
        }
        return $this;
    }

    public function getUrlElement($id, $default = null) {
        if (array_key_exists($id, $this->_urlElements)) {
            return $this->_urlElements[$id];
        }
        return $default;
    }

    public function getUrlElements() {
        return $this->_urlElements;
    }

    protected function setMethod() {
        $this->_method = strtoupper($_SERVER['REQUEST_METHOD']);
        return $this;
    }

    public function getMethod() {
        if (!$this->_method) {
            $this->setMethod();
        }
        return $this->_method;
    }

    protected function setParams() {
        switch ($this->_method) {
            case self::METHOD_GET:
                $params = $_GET;
                break;
            case self::METHOD_POST:
                $params = file_get_contents('php://input');
                break;
            case self::METHOD_PUT:
                $params = file_get_contents('php://input');
                break;
            case self::METHOD_DELETE:
                $params = file_get_contents('php://input');
                break;
            default :
                $params = file_get_contents('php://input');
        }
        $this->_params = $this->_prepareParams($params);
        return $this;
    }

    public function getParams() {
        if (!$this->_params) {
            $this->setParams();
        }
        return $this->_params;
    }

    public function getControllerName() {
        if (array_key_exists(self::CONTROLLER_INDEX, $this->_urlElements)) {
            return strtolower($this->_urlElements[self::CONTROLLER_INDEX]);
        }
        return null;
    }

    public function getActionName() {
        return strtolower($this->getMethod());
    }

    protected function setHeaders() {
        $this->_headers = apache_request_headers();
        return $this;
    }

    public function getHeader($header) {
        if (array_key_exists($header, $this->_headers)) {
            return $this->_headers[$header];
        }
        return null;
    }

    protected function _prepareParams($params) {
        if(is_string($params)){
            $params = (array) json_decode($params);
        }
        if(is_array($params)){
            $filteredParams = array();
            foreach ($params as $key=>$value){
                $filteredParams[strtoupper($key)] = $value;
            }
            $params = $filteredParams;
        }
        return $params;
    }

}