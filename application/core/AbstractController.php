<?php

abstract  class AbstractController {

    protected $view;
    protected $model;
    protected $_request;
    protected $_response;
   
    
    public function setRequest(Request $request){
        $this->_request = $request;
    }
    
    public function getRequest(){
        return $this->_request;
    }    
    
    public function setResponse(Response $response){
        $this->_response = $response;
    }
    
    public function getResponse(){
        return $this->_response;
    } 
    

}