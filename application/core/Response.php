<?php

class Response
{
    protected $_headers = array();

    
    public function __construct() {
        $this->setHeader("Content-Type: application/json");
    }
    
    
    public function send() {
        $this->_sendHeaders();
    }

    public function setHeader($header) {
        $this->_headers[] = $header;
    }

    protected function _sendHeaders() {
        foreach ($this->_headers as $header) {
            header($header);
        }
    }    
}