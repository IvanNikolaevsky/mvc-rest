<?php

class Route {
    
    protected $_controllerName;
    protected $_controller;
    protected $_actionName;
    protected $_request;
    protected $_response;

    public function init() {

        $this->_request = new Request();
        $this->_response = new Response();
    
        if(!$this->isAcceptedType()){
            $this->_response->setHeader('HTTP/1.1 406 Not Acceptable');
            $this->_response->setHeader('Content-Type:application/json');
            $view = new View();
            $view->json(array("accept"=>"application/json"));  
            $this->_response->send();
            return false;
        }
        

        $this->setControllerName();
        
        
        if($this->checkController()){
            $this->_controller = new $this->_controllerName();
            $this->_controller->setRequest($this->_request);
            $this->_controller->setResponse($this->_response);

            $this->setActionName();
            
            if($this->checkAction()){
                $this->_controller->{$this->_actionName}();
            }else{
                $this->_response->setHeader("HTTP/1.1 404 Not Found");
            }
        }else{
            $this->_response->setHeader("HTTP/1.1 404 Not Found");     
        }
        $this->_response->send();
    }
    
    
    protected function isAcceptedType(){
        if($this->_request->getHeader("Accept") === "application/json"){
            return true;
        }           
        return false;
    }


    
    
    
    public function checkController(){
        if (class_exists($this->_controllerName)) {
            return true;
        } 
        return false;     
    }
    
    public function setControllerName(){
        if ($this->_request->getControllerName()) {
            $this->_controllerName = ucfirst($this->_request->getControllerName()) . 'Controller';
        }          
    }
    
    public function setActionName(){
        $this->_actionName = $this->_request->getActionName() . "Action";        
    }
   
    public function checkAction(){
        if (method_exists($this->_controller, $this->_actionName)) {
            return true;
        }        
        return false;
    }


}